package main

import (
	"net"
	"os"

	"github.com/go-kit/kit/log"

	pb "gitlab.com/bbgworkshop/car-locator/grpc"
	"gitlab.com/bbgworkshop/car-locator/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	//car-locator service
	var svc server.CarLocatorService
	svc = server.NewCarLocatorService()

	//middleware
	logger := log.NewLogfmtLogger(os.Stderr)
	svc = server.LoggingMiddleware{Logger: logger, Next: svc}

	//endpoint
	addEp := server.MakeSetGeoEndpoint(svc)
	getEp := server.MakeFindInRadiusEndpoint(svc)

	//transport
	grpcServer := server.NewGRPCServer(addEp, getEp)

	//start grpc
	pbServer := grpc.NewServer()
	pb.RegisterCarLocatorServer(pbServer, grpcServer)
	reflection.Register(pbServer)

	lis, _ := net.Listen("tcp", ":9090")
	pbServer.Serve(lis)
}
