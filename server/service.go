package server

import (
	"context"

	"gitlab.com/bbgworkshop/car-locator/storage"
)

//CarLocatorService is service to manage geo location of a car
type CarLocatorService interface {
	SetGeo(context.Context, storage.Car) (bool, error)
	FindVacantInRadius(context.Context, float64, float64, float32) ([]storage.Car, error)
}

type FindVacantInRadiusRequest struct {
	Longitude	float64
	Latitude	float64
	Radius		float32
}