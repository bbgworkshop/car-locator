package server

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/transport/grpc"
	pb "gitlab.com/bbgworkshop/car-locator/grpc"
	"gitlab.com/bbgworkshop/car-locator/storage"
	grpccontext "golang.org/x/net/context"
)

type grpcCarLocatorService struct {
	setGeo grpc.Handler
	findVacantInRadius grpc.Handler
}

func (s grpcCarLocatorService) SetGeo(ctx grpccontext.Context, request *pb.SetGeoRequest) (*pb.SetGeoResponse, error) {
	_, resp, err := s.setGeo.ServeGRPC(ctx, request)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.SetGeoResponse), nil
}

func (s grpcCarLocatorService) FindVacantInRadius(ctx grpccontext.Context, request *pb.FindVacantInRadiusRequest) (*pb.FindVacantInRadiusResponse, error) {
	_, resp, err := s.findVacantInRadius.ServeGRPC(ctx, request)
	if err != nil {
		return nil, err
	}
	return resp.(*pb.FindVacantInRadiusResponse), nil
}

func decodeSetGeoRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(*pb.SetGeoRequest)
	car := req.GetCar()
	return storage.Car{CarNo: car.GetCarNo(), Longitude: car.GetLongitude(), Latitude: car.GetLatitude(), Status: car.GetStatus()}, nil
}

func encodeSetGeoResponse(_ context.Context, response interface{}) (interface{}, error) {
	res := response.(bool)
	return &pb.SetGeoResponse{Success: res}, nil
}

func decodeFindInRadiusRequest(_ context.Context, request interface{}) (interface{}, error) {
	req := request.(*pb.FindVacantInRadiusRequest)
	return FindVacantInRadiusRequest{Longitude: req.GetLongitude(), Latitude: req.GetLatitude(), Radius: req.GetRadius()}, nil
}

func encodeFindInRadiusResponse(_ context.Context, response interface{}) (interface{}, error) {
	res := response.([]storage.Car)
	cars := make([]*pb.Car, 0)
	for _, r := range res {
		car := pb.Car{CarNo: r.CarNo, Longitude: r.Longitude, Latitude: r.Latitude}
		cars = append(cars, &car)
	}
	return &pb.FindVacantInRadiusResponse{Cars: cars}, nil
}

func makeSetGeoHandler(endpoint endpoint.Endpoint, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoint, decodeSetGeoRequest, encodeSetGeoResponse, options...)
}

func makeFindVacantInRadiusHandler(endpoint endpoint.Endpoint, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoint, decodeFindInRadiusRequest, encodeFindInRadiusResponse, options...)
}

func NewGRPCServer(addEp, getEp endpoint.Endpoint) pb.CarLocatorServer {
	options := []grpc.ServerOption{}
	return &grpcCarLocatorService{
		setGeo:  makeSetGeoHandler(addEp, options),
		findVacantInRadius: makeFindVacantInRadiusHandler(getEp, options),
	}
}
