package server

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
	"gitlab.com/bbgworkshop/car-locator/storage"
)

type LoggingMiddleware struct {
	Logger log.Logger
	Next   CarLocatorService
}

func (l LoggingMiddleware) SetGeo(ctx context.Context, car storage.Car) (success bool, err error) {
	defer func(begin time.Time) {
		l.Logger.Log("method", "SetGeo",
			"input", car,
			"output", success,
			"error", err,
			"took", time.Since(begin))
	}(time.Now())

	success, err = l.Next.SetGeo(ctx, car)
	return
}

func (l LoggingMiddleware) FindVacantInRadius(ctx context.Context, longitude float64, latitude float64, radius float32) (cars []storage.Car, err error) {
	defer func(begin time.Time) {
		l.Logger.Log("method", "FindVacantInRadius",
			"longitude", longitude,
			"latitude", latitude,
			"radius", radius,
			"output", cars,
			"error", err,
			"took", time.Since(begin))
	}(time.Now())

	cars, err = l.Next.FindVacantInRadius(ctx, longitude, latitude, radius)
	return
}
