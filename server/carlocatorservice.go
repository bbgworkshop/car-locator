package server

import (
	"context"

	"gitlab.com/bbgworkshop/car-locator/storage"
)

type simpleCarLocatorService struct {

}

func NewCarLocatorService() CarLocatorService {
	return &simpleCarLocatorService{}
}

func (c *simpleCarLocatorService) SetGeo(ctx context.Context, car storage.Car) (bool, error) {
	return true, nil
}

func (c *simpleCarLocatorService) FindVacantInRadius(ctx context.Context, longitude float64, latitude float64, radius float32) ([]storage.Car, error) {
	return []storage.Car{}, nil
}
