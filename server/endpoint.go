package server

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"gitlab.com/bbgworkshop/car-locator/storage"
)

func MakeSetGeoEndpoint(svc CarLocatorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(storage.Car)
		resp, err := svc.SetGeo(ctx, req)
		if err != nil {
			return false, err
		}
		return resp, nil
	}
}

func MakeFindInRadiusEndpoint(svc CarLocatorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(FindVacantInRadiusRequest)
		resp, err := svc.FindVacantInRadius(ctx, req.Longitude, req.Latitude, req.Radius)
		if err != nil {
			return []storage.Car{}, err
		}
		return resp, nil
	}
}
