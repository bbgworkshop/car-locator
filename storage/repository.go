package storage

type Car struct {
	CarNo     string
	Longitude float64
	Latitude  float64
	Status    int32
}

type RadiusUnit string
const (
	RadiusUnit_Meter 		RadiusUnit = "m"
	RadiusUnit_KiloMeter	RadiusUnit = "km"
	RadiusUnit_Feet			RadiusUnit = "ft"
	RadiusUnit_Mile			RadiusUnit = "ml"
)

type Cache interface {
	GeoAdd(car Car) error
	GeoRadius(longitude float64, latitude float64, radius float32, unit RadiusUnit, status int32) ([]Car, error)
}
