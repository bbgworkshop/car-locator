package main

import (
	"log"

	pb "gitlab.com/bbgworkshop/car-locator/grpc"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial(":9090", grpc.WithInsecure())
	if err != nil {
		log.Printf("ERROR: %v", err)
		return
	}
	client := pb.NewCarLocatorClient(conn)

	car := pb.Car{
		CarNo: "BO1235",
		Longitude:106.842706,
		Latitude:-6.247081, Status:0,
	}

	resp, _ := client.SetGeo(context.Background(), &pb.SetGeoRequest{Car: &car})
	if err != nil {
		log.Printf("ERROR: Set geo location failed: %v", err)
		return
	}
	log.Printf("Set geo location success: %v", resp)

	passenger := pb.FindVacantInRadiusRequest{
		Longitude: 106.82565,
		Latitude: -6.244666,
		Radius:5,
	}

	cars, _ := client.FindVacantInRadius(context.Background(), &passenger)
	if err != nil {
		log.Printf("ERROR: Finding vacant cars in radius: %v", err)
	}
	log.Printf("Cars found: %v", cars)
}